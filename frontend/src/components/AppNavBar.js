import { Container, Nav, Navbar } from 'react-bootstrap';
import { useContext } from 'react';
import { NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

function AppNavBar() {
  // const [user, setUser] = useState(null);
  // useEffect(() => {
  //   setUser(localStorage.getItem('email'));
  // }, [localStorage]);

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg" className="vw-100 overflow-hidden">
      <Container>
        <Navbar.Brand as={NavLink} to="/">
          Course-Booking
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/courses">
              Courses
            </Nav.Link>
            {user.id ? (
              <Nav.Link as={NavLink} to="/logout">
                Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppNavBar;
