import { Col, Button } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';

import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import '../App.css';

// useState hook
// const [get <variable>, set<function>] = useState(initialgetvalue)

const CourseCard = ({ coursesData }) => {
  // const [enrollees, setEnrollees] = useState(0);

  const [isAvailable, setIsAvailable] = useState(true);
  // useEffet hook
  // useEffect(functionWillTriggered, [statesToMonitor])
  // console.log(coursesData.enrollees.length);

  useEffect(() => {
    if (coursesData.slots === 0) {
      setIsAvailable(false);
    }
  }, [coursesData.slots]);

  const { _id, name, description, enrollees, price, slots } = coursesData;

  const { user } = useContext(UserContext);
  return (
    <Col xs={12} lg={4} className="my-3">
      <Card className="p-3 h-100">
        <Card.Body className="d-flex flex-column">
          <Card.Title>
            <strong>{name}</strong>
          </Card.Title>
          <Card.Text>
            <strong>Description:</strong>
            <br />
            {description} <br />
          </Card.Text>
          <Col className="d-flex flex-column justify-content-end">
            <Card.Text>
              <strong>Price:</strong>
              <br />
              <strong>&#8369; {price}</strong>
            </Card.Text>
            <Card.Text>
              <strong>Enrollees:</strong>
              <br />
              <strong>{enrollees.length}</strong>
            </Card.Text>
            <Card.Text>
              <strong>Slot Available:</strong>
              <br />
              <strong>{slots}</strong>
            </Card.Text>
            {user.id ? (
              <Button
                as={Link}
                to={`/courses/${_id}`}
                id={_id}
                className="col-12 col-md-4"
                variant="primary"
                disabled={!isAvailable}
              >
                Enroll
              </Button>
            ) : (
              <Button
                as={Link}
                to={`/courses/${_id}`}
                id={_id}
                className="col-12 col-md-4"
                variant="primary"
                disabled={!isAvailable}
              >
                Details
              </Button>
            )}
          </Col>
        </Card.Body>
      </Card>
    </Col>
  );
};

export default CourseCard;
