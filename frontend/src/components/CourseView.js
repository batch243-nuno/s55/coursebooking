import React, { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

function CourseView() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  //   const [enrollees, setEnrollees] = useState(0);

  const history = useNavigate();

  const { courseID } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseID}`)
      .then((response) => response.json())
      .then((data) => {
        // console.log(data.enrollees.length);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        // setEnrollees(data.enrollees.length);
      });
  }, [courseID]);

  const enroll = (courseID) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/enroll/${courseID}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.enrolled) {
          // console.log(first)
          Swal.fire({
            title: data.message,
            icon: 'success',
            text: 'Goodluck on your Journey',
          });
          history('/courses');
        } else {
          Swal.fire({
            title: 'Enroll Failed!',
            icon: 'error',
            text: data.message,
          });
          history('/');
        }
      });

    // if (coursesData.slots === 1) {
    //   alert('Congratulations! You Are The Last One We Need!');
    // }
    // setEnrollees(enrollees + 1);
    // coursesData.slots--;
  };

  return (
    <Container>
      <Row className="d-flex justify-content-center">
        <Col lg={5}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Class Schedule</Card.Subtitle>
              <Card.Text>8 am - 5 pm</Card.Text>
              <Button onClick={() => enroll(courseID)} variant="primary">
                Enroll
              </Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default CourseView;
