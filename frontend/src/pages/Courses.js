import React from 'react';
import { Container, Row } from 'react-bootstrap';

import CourseCard from '../components/CourseCard';

import { useEffect, useState } from 'react';
// import coursesData from '../data/coursesData';

export default function Courses() {
  //   console.log();

  // localstorage.getItem
  // const local = localStorage.getItem('email');
  // console.log(local);

  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/allActiveCourses`)
      .then((response) => response.json())
      .then((data) => {
        setCourses(data);
      });
  }, []);

  return (
    <Container>
      <Row>
        {courses.map((course) => (
          <CourseCard key={course._id} coursesData={course} />
        ))}
      </Row>
    </Container>
  );
}
