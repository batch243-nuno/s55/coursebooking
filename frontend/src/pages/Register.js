import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useContext } from 'react';

function Register() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setmobileNo] = useState('');
  const [password, setPassword] = useState('');
  const [rePassword, setRePassword] = useState('');
  const [disable, setDisable] = useState(true);

  const [firstNameValidation, setFirstNameValidation] = useState('invalid');
  const [lastNameValidation, setLastNameValidation] = useState('invalid');
  const [mobileNoValidation, setMobileNoValidation] = useState('invalid');

  const { setUser } = useContext(UserContext);

  const history = useNavigate();

  useEffect(() => {
    if (
      firstName.length >= 11 &&
      lastName.length >= 11 &&
      mobileNo.length >= 11 &&
      email &&
      password &&
      rePassword &&
      password === rePassword
    ) {
      setDisable(false);
      setFirstNameValidation('valid');
      setLastNameValidation('valid');
      setMobileNoValidation('valid');

      // alert('Successfully Registered');

      // setDisable(true);
      // alert('Password Not Match!');
    } else {
      setDisable(true);
      setFirstNameValidation('invalid');
      setLastNameValidation('invalid');
      setMobileNoValidation('invalid');
    }
  }, [firstName, lastName, mobileNo, email, password, rePassword]);

  // useEffect(() => {
  //   firstName.length >= 11
  //     ?
  //     :

  //   lastName.length >= 11
  //     ?
  //     :

  //   mobileNo.length >= 11
  //     ?
  //     :
  // }, [firstName, lastName, mobileNo]);

  // const { setUser } = useContext(UserContext);

  const registerHandler = (event) => {
    event.preventDefault();

    // alert(`${email} is Successfully Registered`);

    // localStorage.setItem('email', email);
    // setUser(localStorage.getItem('email'));

    fetch('http://localhost:4000/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        password,
        mobileNo,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.status) {
          localStorage.setItem('accessToken', data.accessToken);
          retreiveUserDetails(data.accessToken);
          Swal.fire({
            title: 'Registered Successfully!',
            icon: 'success',
            text: data.message,
          });
          history('/');
        } else {
          Swal.fire({
            title: 'Registration Failed!',
            icon: 'error',
            text: data.message,
          });
          history('/');
        }
      });
  };

  const retreiveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUser({ id: data._id, isAdmin: data.isAdmin });
      });
  };

  return (
    <Container fluid>
      <Row className="">
        <Col className="d-flex justify-content-center">
          <Form
            onSubmit={registerHandler}
            className="col-12 col-md-6 m-5 was-validated"
          >
            <Form.Group className="mb-3 d-flex align-items-center">
              <Form.Label className="col-2">First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Last Name"
                value={firstName}
                onChange={(event) => setFirstName(event.target.value)}
                required
              />
              {firstNameValidation === 'invalid' ? (
                <div className="invalid-feedback">
                  Must be Atleat 11 Characters!
                </div>
              ) : (
                <>
                  <div className="valid-feedback">Looking Good !</div>
                </>
              )}
              <Form.Label className="col-2">Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Last Name"
                value={lastName}
                onChange={(event) => setLastName(event.target.value)}
                required
              />
              {lastNameValidation === 'invalid' ? (
                <div className="invalid-feedback">
                  Must be Atleat 11 Characters!
                </div>
              ) : (
                <>
                  <div className="valid-feedback">Looking Good !</div>
                </>
              )}
            </Form.Group>
            <Form.Group className="mb-3 d-flex align-items-center">
              <Form.Label className="col-2">Email address</Form.Label>
              <Form.Control
                className="me-3 "
                type="email"
                placeholder="Enter Email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
              <Form.Label className="col-2">Mobile Number</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Mobile Number"
                value={mobileNo}
                onChange={(event) => setmobileNo(event.target.value)}
                required
              />
              {mobileNoValidation === 'invalid' ? (
                <div className="invalid-feedback">
                  Must be Atleat 11 Characters!
                </div>
              ) : (
                <>
                  <div className="valid-feedback">Looking Good !</div>
                </>
              )}
            </Form.Group>
            <div className="d-flex flex-column align-items-center">
              <Form.Group className="mb-3 col-6" controlId="formPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter Password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3 col-6" controlId="formRePassword">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Re-Enter Password"
                  value={rePassword}
                  onChange={(event) => setRePassword(event.target.value)}
                  required
                />
              </Form.Group>
              <Button
                className="col-8 mt-3"
                variant="primary"
                type="submit"
                disabled={disable}
              >
                Register
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Register;
