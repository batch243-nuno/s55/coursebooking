import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

function Logout() {
  const { setUser, unSetUser } = useContext(UserContext);
  unSetUser();
  useEffect(() => {
    setUser({ id: null, isAdmin: false });
  });
  Swal.fire({
    title: `See You Again Later`,
    html: `GoodLuck to Your Journey`,
    icon: 'info',
  });

  return <Navigate to="/login" />;
}

export default Logout;
