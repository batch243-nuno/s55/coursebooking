import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';

// import './css/pagenotfound.css';

function PageNotFound() {
  return (
    <Container>
      <Row>
        <Col className="d-flex flex-column align-items-center">
          <h1>Page Not Found</h1>
          <h4 className="text-muted">Error404</h4>
          <p>
            Go Back to <Link to="/">Homepage</Link>
          </p>
        </Col>
      </Row>

      {/* <div className="error">
        <div className="wrap">
          <div className="404 text-white">
            <pre>
              <code>
                <span className="green">&lt;!</span>
                <span>DOCTYPE html</span>
                <span className="green">&gt;</span>
                <br />
                <span className="orange">&lt;html&gt;</span>
                <br />
                <span className="orange"> &lt;style&gt;</span>
                <br /> * &#123; <br />
                <span className="green"> everything</span>&#58;
                <span className="blue">awesome</span>;<br /> &#125; <br />
                <span className="orange"> &lt;/style&gt;</span> <br />
                <span className="orange"> &lt;body&gt;</span> <br />
                ERROR 404! FILE NOT FOUND!
                <br />
                <span className="comment">
                  &lt;!--The file you are looking for, is not where you think it
                  is.--&gt;
                </span>
                <br />
                <span className="orange">&nbsp;&lt;/body&gt;</span>
                <br />
                <span className="orange">&lt;/html&gt;</span>
              </code>
            </pre>
          </div>
        </div>
      </div> */}
    </Container>
  );
}

export default PageNotFound;
